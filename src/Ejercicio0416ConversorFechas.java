/**
 * Fichero: Ejercicio0605.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 16-dic-2013
 */
public class Ejercicio0416ConversorFechas {

  public String normalToAmericano(String s) {
    return s.substring(3, 5) + "/" + s.substring(0, 2) + "/" + s.substring(6);
  }

  public String americanoToNormal(String s) {
    return s.substring(3, 5) + "/" + s.substring(0, 2) + "/" + s.substring(6);
  }

  public static void main(String[] args) {
    Ejercicio0416ConversorFechas s = new Ejercicio0416ConversorFechas();
    System.out.println(s.americanoToNormal("16/08/1973"));
    System.out.println(s.normalToAmericano("08/16/1973"));
  }
}

/* EJECUCION:
 08/16/1973
 16/08/1973
 */
