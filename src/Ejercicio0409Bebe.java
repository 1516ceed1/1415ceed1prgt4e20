/**
 * Fichero: Ejercicio0512.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 26-nov-2013
 */
public class Ejercicio0409Bebe { // Bebe

  Ejercicio0409Bebe(int i) {
    this("Soy un bebe consentido");
    System.out.println("Hola tengo " + i + " mensajes");
  }

  Ejercicio0409Bebe(String s) {
    System.out.println(s);
  }

  void berrea() {
    System.out.println("Buaaaaaaaaaaaaa");
  }

  public static void main(String[] args) {
    new Ejercicio0409Bebe(8).berrea();
  }

}
