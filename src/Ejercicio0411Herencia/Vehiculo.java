/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicio0411Herencia;

/**
 * Fichero: Vehiculo.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 02-dic-2013
 */
public class Vehiculo {

  private int id;
  private static int contador = 0;
  protected String matricula;

  Vehiculo() {
    contador++;
    id = contador;
    matricula = "A";
  }

  /**
   * @return the id
   */
  public int getId() {
    return id;
  }

  /**
   * @return the matricula
   */
  public String getMatricula() {
    return matricula;
  }
}
