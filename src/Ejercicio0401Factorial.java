/**
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */

public class Ejercicio0401Factorial {
  /*
   fac(1)=1
   fac(n)=fac(n-1)*n
   */

  /*
   * Factorial Recursivo 
   */
  public static int factorialr(int n) {
    if (n == 1) {
      return 1;
    } else {
      return factorialr(n - 1) * n;
    }
  }

  /*
   * Factorial Iterativo
   */
  public static int factoriali(int n) {

    int f = 1, i = 1;

    while (i <= n) {
      f = f * i;
      i++;
    }
    return f;
  }

  public static void main(String[] args) {

    System.out.println("Factorial Recursivo de 3 es: " + factorialr(3));
    System.out.println("Factorial Iterativo de 3 es: " + factoriali(3));
  }
}
/* EJECUCION:
 Factorial Recursivo de 3 es: 6
 Factorial Iterativo de 3 es: 6
 */
