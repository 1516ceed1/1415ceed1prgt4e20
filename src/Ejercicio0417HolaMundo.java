/**
 * Fichero: Ejercicio0606.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 16-dic-2013
 */
public class Ejercicio0417HolaMundo {

  public static void main(String[] args) {
    // final String s1 = new String(" Hola "); // Error
    String s1 = new String("Hola ");
    String s2 = new String(" Mundo");
    s1 = s1 + s2;
    System.out.println(s1);
  }
}
