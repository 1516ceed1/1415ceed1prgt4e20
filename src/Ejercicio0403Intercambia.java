/**
 * Fichero: Ejercicio0503.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 15-nov-2013
 */
public class Ejercicio0403Intercambia {

  public static void swap(int[] a, int[] b) {
    int x;
    x = a[0];
    a[0] = b[0];
    b[0] = x;
  }

  public static void main(String[] args) {
    int[] x = {1};
    int[] y = {2};
    System.out.println("x: " + x[0] + "  y: " + y[0]);
    swap(x, y);
    System.out.println("x: " + x[0] + "  y: " + y[0]);
  }
}

/*EJECUCION:
 x: 1  y: 2
 x: 2  y: 1
 */
