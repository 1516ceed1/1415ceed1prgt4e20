/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicio0410aInterfaceGenerico;

import Ejercicio0410Interface.*;

/**
 * Fichero: Main.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 28-nov-2013
 */
public class Main {

    public static void main(String argv[]) {

        BaseDatos<Oracle> bd = new Oracle();
        Singleton s1;
        Singleton s2;
        s1 = Singleton.getSingleton("Titulo1");
        s2 = Singleton.getSingleton("Titulo2");
        System.out.println(s1.getTitulo());
        System.out.println(s2.getTitulo());
        bd.open();
        bd.close();

    }
}
