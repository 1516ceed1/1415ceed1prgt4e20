/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicio0410aInterfaceGenerico;

import Ejercicio0410Interface.*;

/**
 * Fichero: Mysql.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 28-nov-2013
 */
public class Oracle implements BaseDatos {

  @Override
  public void open() {
    Constantes c = new Constantes();
    System.out.println("Abierta conexión a oracle.");
    System.out.println("Usuario: " + c.USUARIOORACLE);
    System.out.println("Contraseña: " + c.CONSTRASENYAORACLE);
  }

  @Override
  public void close() {
    System.out.println("Cerrada conexión a oracle");
  }
}
