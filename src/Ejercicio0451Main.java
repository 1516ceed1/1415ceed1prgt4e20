//Importamos la libreria donde se halla la clase Point
import java.awt.Point;

public class Ejercicio0451Main {
  /*
   Usaremos la clase Ejercicio0504 y su superclase: Point en esta aplicaciOn
   */

  public static void main(String args[]) {
    //Creamos un objeto de la clase Point
    Point objeto1 = new Point(1, 24);
    //Creamos un objeto de la clase Ejercicio0450
    Ejercicio0450 objeto2 = new Ejercicio0450(12, 1, 21);
    //A continuaciOn la parte del objeto de tipo Point (2D).
    System.out.println("El punto esta localizado en (" + objeto1.x + ", "
            + objeto1.y + ").");
    System.out.println("Esta siendo movido a (3, 16).");
    objeto1.move(3, 16);
    System.out.println("El punto esta localizado en (" + objeto1.x + ", "
            + objeto1.y + ").");
    System.out.println("Esta siendo movido +10 en el eje x.");
    objeto1.translate(+10, 0);
    System.out.println("El punto esta localizado en (" + objeto1.x + ", "
            + objeto1.y + ").");
    System.out.println("Esta siendo movido +5 en eje x y -4 en eje y.");
    objeto1.translate(+5, -4);
    System.out.println("El punto esta localizado en (" + objeto1.x + ", "
            + objeto1.y + ").");

//A continuaciOn la parte del objeto de tipo Ejercicio0504.
    System.out.println("");
    System.out.println("El punto esta localizado en (" + objeto2.x + ", "
            + objeto2.y + ", " + objeto2.z + ").");
    System.out.println("Esta siendo movido a (3, 10, 1).");
    objeto2.move(3, 10, 1);
    System.out.println("El punto esta localizado en (" + objeto2.x + ", "
            + objeto2.y + ", " + objeto2.z + ").");
    System.out.println("Esta siendo movido +10 en el eje x.");
    objeto2.translate(10, 0, 0);
    System.out.println("El punto esta localizado en (" + objeto2.x + ", "
            + objeto2.y + ", " + objeto2.z + ").");
    System.out.println("Esta siendo movido +1 en eje x y -5 en ejes y, z.");
    objeto2.translate(+1, -5, -5);
    System.out.println("El punto esta localizado en (" + objeto2.x + ", "
            + objeto2.y + ", " + objeto2.z + ").");
  }
}

/* EJECUCION:
 El punto esta localizado en (1, 24).
 Esta siendo movido a (3, 16).
 El punto esta localizado en (3, 16).
 Esta siendo movido +10 en el eje x.
 El punto esta localizado en (13, 16).
 Esta siendo movido +5 en eje x y -4 en eje y.
 El punto esta localizado en (18, 12).

 El punto esta localizado en (12, 1, 21).
 Esta siendo movido a (3, 10, 1).
 El punto esta localizado en (3, 10, 1).
 Esta siendo movido +10 en el eje x.
 El punto esta localizado en (13, 10, 1).
 Esta siendo movido +1 en eje x y -5 en ejes y, z.
 El punto esta localizado en (14, 5, -4).
 */
