/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicio0410Interface;

/**
 *
 * Fichero: BaseDatos.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 24-nov-2015
 */
public interface BaseDatos {

    void close();

    void open();

}
